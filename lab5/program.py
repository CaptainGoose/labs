# coding: utf-8
class Bag:
    def __init__(self):
        self.capacity = None
        self.item = []
        self.size = []

    def put_in_bag(self, size, item):
        if size < self.capacity:
            self.item.append(item)
            self.size.append(size)

    def can_i_put(self, size):
        if size < self.capacity:
            return True
        else:
            return False

    def what_in_bag(self):
            print (self.item, self.size)

class Item:
    def __init__(self):
        self.name = None
        self.size = None

    def print_yourself(self):
        print (self.name, self.size)

b = Bag()
i = Item()

b.capacity = 10

i.name = 'iPhone'
i.size = 5

print "Напечатай себя, вещь!"
i.print_yourself()

print "Можем положить айфон в сумку?"
if b.can_i_put(i.size):
    print "Конечно можем"
else:
    print "Неа, не можем"

print "Давайте положим айфон в сумку"
b.put_in_bag(i.size, i.name)

i2 = Item()
i2.name = 'kakashka'
i2.size = 1

b.put_in_bag(i2.size, i2.name)

print "Что теперь в сумке?"
b.what_in_bag()
